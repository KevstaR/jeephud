import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.9.22"
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_21.majorVersion
        }
    }
}

allprojects {
    group = "com.kluke.jeephud"
    version = "1.0-SNAPSHOT"
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    // ==== Main ====
    implementation(project(":shared"))

    // ==== Testing ====
    val mockitoVersion = "5.8.0"
    val junitVersion = "5.10.1"

    // Testing Framework
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    // Mocking
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
    testImplementation("org.mockito:mockito-junit-jupiter:$mockitoVersion")

    // Assertions
    testImplementation("org.assertj:assertj-core:3.24.2")
}