package com.kluke.jeephud.frontend

import com.kluke.jeephud.shared.data.RoadSpeedDataFrame
import java.io.DataInputStream
import java.io.IOException
import java.net.Socket
import java.util.logging.Logger


fun main() {
    val logger = Logger.getLogger("Main")
    try {
        Socket("localhost", 9090).use { socket ->
            val input = socket.getInputStream()
            val inputStream = DataInputStream(input)
            while (true) {
                when (inputStream.readUTF()) {
//                    RoadSpeedDataFrame.ID -> {
//                        val frame = RoadSpeedDataFrame(inputStream.readDouble(), inputStream.readUTF())
//
//                         ...
//                    }
                }
            }
        }
    } catch (ex: IOException) {
        logger.severe(ex.message)
    }
}
