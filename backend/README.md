# Jeep Heads Up Display - Backend

## Project Scope
        The goal of this project is to rewrite and separate the sensor and instrument layer of the implementation. This will
        keep UI code in the UI and backend code easy to debug and test.

## Primary Goals
        1. Simple input to output tests via mock sensor data.
2. Parallelize the start time of the backend and front end to cut down on app overhead.
3. Clean API and separation of outputs allowing easy implementation and maintenance of features.