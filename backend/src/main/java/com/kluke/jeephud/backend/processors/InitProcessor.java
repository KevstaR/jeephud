package com.kluke.jeephud.backend.processors;

import com.kluke.jeephud.backend.database.InstrumentVariableInitializer;

/**
 * The init processor should be run after classes are created in the context and after subscriptions are completed,
 * but can be before start is called on relevant components.
 */
public class InitProcessor implements Processor {
    private final InstrumentVariableInitializer instrumentVariableInitializer;

    public InitProcessor(InstrumentVariableInitializer instrumentVariableInitializer) {
        this.instrumentVariableInitializer = instrumentVariableInitializer;
    }

    @Override
    public void run() {
        instrumentVariableInitializer.start();
    }
}
