package com.kluke.jeephud.backend.database.templates;

public interface OdometerInstrumentQueryTemplate {
    String getInitializationQuery();
    String getTerminationQuery(String odometerValue);
}
