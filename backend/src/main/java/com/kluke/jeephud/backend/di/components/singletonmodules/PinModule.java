package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.pi4j.pins.Pin;
import com.kluke.jeephud.backend.pi4j.pins.Pins;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalInputConfig;
import com.pi4j.io.gpio.digital.DigitalInputProvider;
import com.pi4j.io.gpio.digital.PullResistance;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public abstract class PinModule {
    @Provides
    @Singleton
    @Named("roadSpeedPin")
    static DigitalInput provideRoadSpeedPin(DigitalInputProvider provider, Context pi4j) {
        return provider.create(createConfig(pi4j, new Pin(Pins.ROAD_SPEED_PIN, PullResistance.PULL_UP, 4)));
    }

    private static DigitalInputConfig createConfig(Context pi4j, Pin pin) {
        return DigitalInput.newConfigBuilder(pi4j)
                .id(pin.id().name())
                .address(pin.address())
                .pull(pin.pullResistance())
                .build();
    }
}
