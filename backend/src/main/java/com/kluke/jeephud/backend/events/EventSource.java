package com.kluke.jeephud.backend.events;

public interface EventSource {
    void notifyObservers();
    void addObserver(Runnable observer);
}
