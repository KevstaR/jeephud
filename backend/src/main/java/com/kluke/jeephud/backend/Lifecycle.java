package com.kluke.jeephud.backend;

public interface Lifecycle {
    void start();
    default void stop() {

    }
}
