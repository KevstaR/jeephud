package com.kluke.jeephud.backend.socket;

import java.io.ObjectOutputStream;

public interface ApiStream {
    ObjectOutputStream getOutputStream();
    void close();
}
