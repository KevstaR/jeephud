package com.kluke.jeephud.backend.datastructures;

import java.util.ArrayList;

public class EvictingArrayList<E> extends ArrayList<E> {
    private final int maxCapacity;

    public EvictingArrayList(int maxCapacity) {
        super(maxCapacity);
        this.maxCapacity = maxCapacity;
    }

    @Override
    public boolean add(E e) {
        checkMaxCapacity();
        return super.add(e);
    }

    @Override
    public void addFirst(E e) {
        checkMaxCapacity();
        super.addFirst(e);
    }

    @Override
    public void addLast(E e) {
        checkMaxCapacity();
        super.addLast(e);
    }

    private void checkMaxCapacity() {
        if (size() >= maxCapacity) removeLast();
    }
}
