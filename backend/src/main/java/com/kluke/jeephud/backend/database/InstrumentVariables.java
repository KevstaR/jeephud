package com.kluke.jeephud.backend.database;

public record InstrumentVariables(long rawOdometerValue) {}
