package com.kluke.jeephud.backend.socket;

import javax.inject.Inject;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

public class SocketApi implements ApiStream {
    private static final Logger LOGGER = Logger.getLogger("SocketProvider");
    private final ServerSocket serverSocket;

    @Inject
    public SocketApi() {
        try {
            serverSocket = new ServerSocket(9090);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Blocks until socket connection is established.
     */
    @Override
    public ObjectOutputStream getOutputStream() {
        LOGGER.info("Waiting for Server Socket Connection...");
        try {
            final Socket socket = serverSocket.accept();
            LOGGER.info("Server Socket Connection established!");
            return new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            serverSocket.close();
            LOGGER.info("Server Socket Connection successfully closed!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
