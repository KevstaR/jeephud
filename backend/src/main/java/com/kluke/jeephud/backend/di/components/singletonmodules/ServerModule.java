package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.Server;
import com.kluke.jeephud.backend.ServerImpl;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface ServerModule {
    @Binds
    @Singleton
    Server bindServer(ServerImpl impl);
}
