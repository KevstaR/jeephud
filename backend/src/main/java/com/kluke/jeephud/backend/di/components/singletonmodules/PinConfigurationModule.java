package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.IOType;
import com.pi4j.io.gpio.digital.DigitalInputProvider;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public abstract class PinConfigurationModule {
    @Provides
    @Singleton
    static Context provideContext() {
        return Pi4J.newAutoContext();
    }

    @Provides
    @Singleton
    static DigitalInputProvider provideDigitalInputProvider(Context pi4j) {
        return pi4j.provider(IOType.DIGITAL_INPUT);
    }
}
