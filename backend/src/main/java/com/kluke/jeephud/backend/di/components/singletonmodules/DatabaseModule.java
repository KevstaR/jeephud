package com.kluke.jeephud.backend.di.components.singletonmodules;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Module
public abstract class DatabaseModule {
    @Provides
    @Singleton
    static Connection provideConnection(@Named("databaseUrl") String databaseUrl) {
        try {
            return DriverManager.getConnection(databaseUrl);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
