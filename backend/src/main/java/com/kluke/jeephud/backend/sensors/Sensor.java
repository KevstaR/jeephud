package com.kluke.jeephud.backend.sensors;

import com.kluke.jeephud.backend.Lifecycle;

public interface Sensor<T> extends Lifecycle {
    T getData();
}
