package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.services.*;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface ServiceModule {
    @Binds
    @Singleton
    TickService provideStandardTickService(StandardTickService impl);

    @Binds
    @Singleton
    EventPublisher provideEventPublisher(EventPublisherImpl impl);
}