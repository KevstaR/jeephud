package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.instruments.InstrumentManager;
import com.kluke.jeephud.backend.instruments.impl.InstrumentManagerImpl;
import com.kluke.jeephud.backend.services.ServiceManager;
import com.kluke.jeephud.backend.services.ServiceManagerImpl;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface ManagerModule {
    @Binds
    @Singleton
    ServiceManager bindServiceManager(ServiceManagerImpl impl);

    @Binds
    @Singleton
    InstrumentManager bindInstrumentManager(InstrumentManagerImpl impl);
}
