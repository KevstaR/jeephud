package com.kluke.jeephud.backend.sensors.impl;

import com.kluke.jeephud.backend.datastructures.EvictingArrayList;
import com.kluke.jeephud.backend.events.EventProvider;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.pi4j.io.gpio.digital.DigitalInput;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

public class RoadSpeedSensor implements Sensor<List<Long>> {

    /**
     * This size was calculated generously using a max roadspeed of 100MPH and deriving the max number of pulses in
     * one second of sensor data capture.
     * <p>
     * var speedConversionFactor = 1.125F;
     * var maxSpeedMph = 100.0F;
     * <p>
     * var maxDelta = maxSpeedMph / speedConversionFactor;
     * <p>
     * var maxDeltaPerSecond = maxDelta * 5;
     * <p>
     * Result: 444.44443
     * <p>
     * Meaning the planned implementation of this sensor results in the queue being purged of 200ms worth of entries
     * every 200ms.
     */
    private static final int DEQUE_SIZE = 512;

    private final DigitalInput pin;
    private final EventProvider eventProvider;

    private final List<Long> pinEvents;
    
    private boolean initialized = false;

    @Inject
    public RoadSpeedSensor(@Named("roadSpeedPin") DigitalInput pin,
                           @Named("millisecondEventProvider") EventProvider eventProvider) {
        this.pin = pin;
        this.eventProvider = eventProvider;
        this.pinEvents = new EvictingArrayList<>(DEQUE_SIZE);
    }

    @Override
    public void start() {
        if (!initialized) {
            initialized = true;

            pin.addListener(_ -> addEvent());
        }
    }

    private void addEvent() {
        final var event = eventProvider.getEvent();
        pinEvents.add(event);
    }

    @Override
    public List<Long> getData() {
        return pinEvents;
    }
}
