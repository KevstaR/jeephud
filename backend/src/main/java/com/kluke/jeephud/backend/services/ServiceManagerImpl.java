package com.kluke.jeephud.backend.services;

import javax.inject.Inject;
import java.util.logging.Logger;

public class ServiceManagerImpl implements ServiceManager {
    private static final Logger LOGGER = Logger.getLogger("ServiceManagerImpl");
    private final TickService tickService;

    @Inject
    public ServiceManagerImpl(TickService tickService) {
        this.tickService = tickService;
    }

    @Override
    public void startServices() {
        LOGGER.info("Starting services...");
        tickService.start();
    }

    @Override
    public void stopServices() {
        LOGGER.info("Stopping services.");
        tickService.stop();
    }
}
