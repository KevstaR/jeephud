package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.instruments.InstrumentManager;
import com.kluke.jeephud.backend.instruments.OdometerInstrument;
import com.kluke.jeephud.backend.instruments.RoadSpeedInstrument;

import javax.inject.Inject;
import java.util.logging.Logger;

public class InstrumentManagerImpl implements InstrumentManager {
    private static final Logger LOGGER = Logger.getLogger("InstrumentManagerImpl");
    private final OdometerInstrument odometerInstrument;
    private final RoadSpeedInstrument roadSpeedInstrument;

    @Inject
    public InstrumentManagerImpl(OdometerInstrument odometerInstrument, RoadSpeedInstrument roadSpeedInstrument) {
        this.odometerInstrument = odometerInstrument;
        this.roadSpeedInstrument = roadSpeedInstrument;
    }

    @Override
    public void startInstruments() {
        LOGGER.info("Starting Instruments...");
        odometerInstrument.start();
        roadSpeedInstrument.start();
    }

    @Override
    public void stopInstruments() {
        LOGGER.info("Stopping Instruments.");
        odometerInstrument.stop();
        roadSpeedInstrument.stop();
    }
}
