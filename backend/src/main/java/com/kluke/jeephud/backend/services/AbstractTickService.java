package com.kluke.jeephud.backend.services;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public abstract class AbstractTickService implements TickService {

    private boolean initialized = false;
    private final long delay;
    private final ScheduledExecutorService scheduler;
    private final ExecutorService service;
    public final Set<Runnable> observers;

    /**
     * Service schedules a task that runs on its own virtual thread.
     * @param delayMs how long in milliseconds between each task.
     */
    public AbstractTickService(long delayMs) {
        this.delay = delayMs;
        this.scheduler = Executors.newSingleThreadScheduledExecutor();
        this.service = Executors.newVirtualThreadPerTaskExecutor();
        this.observers = new HashSet<>();
    }

    @Override
    public void start() {
        if (!initialized) {
            scheduler.scheduleWithFixedDelay(this::serviceExecute, delay, delay, MILLISECONDS);
            initialized = true;
        }
    }

    private void serviceExecute() {
        service.execute(this::notifyObservers);
    }

    @Override
    public void notifyObservers() {
        for (Runnable observer : observers) {
            observer.run();
        }
    }

    @Override
    public void addObserver(Runnable observer) {
        observers.add(observer);
    }
}
