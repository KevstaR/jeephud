package com.kluke.jeephud.backend.database;


import com.kluke.jeephud.backend.database.templates.OdometerInstrumentQueryTemplate;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Logger;

/**
 * This class works as follows:
 * <P>
 * 1. Sensors implement this class and subscribe via `addObserver`.
 * 2. This class gets run at startup and spawns a virtual thread that performs a query of all relevant instrument data.
 * 3. Once the query is complete, the subscribers are notified. Subscribers will set their relevant values.
 * Subscribers should subscribe in their constructors, prior to their start commands.
 */
public class InstrumentVariableInitializerImpl implements InstrumentVariableInitializer {
    private static final Logger LOGGER = Logger.getLogger("InstrumentVariableInitializer");

    private final Connection connection;
    private final OdometerInstrumentQueryTemplate queryTemplate;
    private final Set<Consumer<InstrumentVariables>> observers;

    @Inject
    public InstrumentVariableInitializerImpl(Connection connection, OdometerInstrumentQueryTemplate queryTemplate) {
        this.connection = connection;
        this.queryTemplate = queryTemplate;
        this.observers = new HashSet<>();
    }

    private InstrumentVariables getFromDatabase() {
        // All Queries
        final String odometerQuery = queryTemplate.getInitializationQuery();
        final long rawOdometerValue = getVariableLong(odometerQuery);

        return new InstrumentVariables(rawOdometerValue);
    }

    private long getVariableLong(String query) {
        return getVariableLong(query, "value");
    }

    private long getVariableLong(String query, String columnLabel) {
        try (final var statement = connection.createStatement();
             final var resultSet = statement.executeQuery(query)) {

            // Check if result set is empty
            return !resultSet.isBeforeFirst() ? 0L : resultSet.getLong(columnLabel);

        } catch (SQLException e) {
            LOGGER.severe(STR."Unable to retrieve long value using query\{query}");
            return 0L;
        }
    }

    @Override
    public void notifyObservers(InstrumentVariables instrumentVariables) {
        for (Consumer<InstrumentVariables> observer : observers) {
            observer.accept(instrumentVariables);
        }
    }

    @Override
    public void addObserver(Consumer<InstrumentVariables> observer) {
        observers.add(observer);
    }

    @Override
    public Thread start() {
        return Thread.ofVirtual().start(this::run);
    }

    private void run() {
        final InstrumentVariables query = getFromDatabase();
        notifyObservers(query);
    }

    @Override
    public void stop() {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
