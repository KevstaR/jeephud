package com.kluke.jeephud.backend.events;

import java.util.function.Consumer;

public interface ConsumerEventSource<T> {
    void notifyObservers(T t);
    void addObserver(Consumer<T> observer);
}
