package com.kluke.jeephud.backend.sensors.impl;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.pi4j.io.gpio.digital.DigitalInput;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Total count sensor is responsible for tracking how many state changes have occurred while the app is running.
 */
public class TotalCountSensor implements Sensor<Long> {

    private final DigitalInput pin;
    private long pinEvents;
    private boolean initialized = false;

    @Inject
    public TotalCountSensor(@Named("roadSpeedPin") DigitalInput pin) {
        this.pin = pin;
    }

    @Override
    public void start() {
        if (!initialized) {
            initialized = true;

            pin.addListener(_ -> pinEvents += 1);
        }
    }

    @Override
    public Long getData() {
        return pinEvents;
    }
}
