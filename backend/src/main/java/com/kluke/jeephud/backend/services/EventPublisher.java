package com.kluke.jeephud.backend.services;

import java.io.IOException;

public interface EventPublisher {
    void poll() throws InterruptedException, IOException;
    void addEvent(Record record);
    void stop();
}
