package com.kluke.jeephud.backend.pi4j.pins;

import com.pi4j.io.gpio.digital.PullResistance;

public record Pin(Pins id, PullResistance pullResistance, int address) {}
