package com.kluke.jeephud.backend.processors;

public interface Processor {
    void run();
}
