package com.kluke.jeephud.backend.events;

import javax.inject.Inject;
import java.time.Duration;

import static java.lang.System.nanoTime;

public class MillisecondEventProvider implements EventProvider {

    @Inject
    public MillisecondEventProvider() {}

    @Override
    public long getEvent() {
        return Duration.ofNanos(nanoTime()).toMillis();
    }
}
