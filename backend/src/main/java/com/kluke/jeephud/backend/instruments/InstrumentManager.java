package com.kluke.jeephud.backend.instruments;

public interface InstrumentManager {
    void startInstruments();
    void stopInstruments();
}
