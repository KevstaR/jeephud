package com.kluke.jeephud.backend.di.components.singletonmodules;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public class PropertiesModule {
    @Provides
    @Singleton
    @Named("databaseUrl")
    String provideDatabaseUrl() {
        return "jdbc:sqlite:main_db";
    }
}
