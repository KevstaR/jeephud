package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.shared.data.RoadSpeedDataFrame;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import static com.kluke.jeephud.backend.datastructures.DataStructureUtil.queryDeltaCount;

public class RoadSpeedTask implements Runnable {
    /**
     * Derived value from real driving data.
     */
    private static final float MPH_COEFFICIENT = 1.125F;

    private final Sensor<List<Long>> roadSpeedSensor;
    private final EventPublisher publisherService;

    public RoadSpeedTask(Sensor<List<Long>> roadSpeedSensor, EventPublisher publisherService) {
        this.roadSpeedSensor = roadSpeedSensor;
        this.publisherService = publisherService;
    }

    @Override
    public void run() {
        RoadSpeedDataFrame record = new RoadSpeedDataFrame(computeMilesPerHour(), ZonedDateTime.now().toString());
        publisherService.addEvent(record);
    }

    private double computeMilesPerHour() {
        return queryDeltaCount(roadSpeedSensor.getData(), 200) * MPH_COEFFICIENT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoadSpeedTask that = (RoadSpeedTask) o;
        return Objects.equals(roadSpeedSensor, that.roadSpeedSensor) && Objects.equals(publisherService, that.publisherService);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roadSpeedSensor, publisherService);
    }
}
