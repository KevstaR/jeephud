package com.kluke.jeephud.backend.services;

public interface ServiceManager {
    void startServices();
    void stopServices();
}
