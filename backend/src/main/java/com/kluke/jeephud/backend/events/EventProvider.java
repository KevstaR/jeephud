package com.kluke.jeephud.backend.events;

public interface EventProvider {
    long getEvent();
}
