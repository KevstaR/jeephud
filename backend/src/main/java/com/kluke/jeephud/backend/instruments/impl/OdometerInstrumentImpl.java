package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.database.InstrumentVariableInitializer;
import com.kluke.jeephud.backend.database.templates.OdometerInstrumentQueryTemplateImpl;
import com.kluke.jeephud.backend.instruments.OdometerInstrument;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.TickService;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicLong;


public class OdometerInstrumentImpl implements OdometerInstrument {
    /**
     * Represents the total count of state changes that have ever occurred while this application has been installed
     * in the vehicle and running.
     * <P>
     * This value will be loaded and saved at application start and stop by the InstrumentVariableInitializer into a
     * database.
     * <P>
     * This value is only ever loaded at start and then a new value is computed and saved at stop.
     */
    private final AtomicLong appLifetimeTotalCount = new AtomicLong(0L);

    /**
     * Live total state change count during the application's runtime.
     */
    private final Sensor<Long> totalCountSensor;

    private final TickService tickService;
    private final EventPublisher publisherService;

    private final Connection connection;

    @Inject
    public OdometerInstrumentImpl(InstrumentVariableInitializer instrumentVariableInitializer,
                                  @Named("totalCountSensor") Sensor<Long> totalCountSensor,
                                  TickService tickService,
                                  EventPublisher publisherService,
                                  Connection connection) {
        instrumentVariableInitializer.addObserver((values) -> appLifetimeTotalCount.set(values.rawOdometerValue()));
        this.totalCountSensor = totalCountSensor;
        this.tickService = tickService;
        this.publisherService = publisherService;
        this.connection = connection;
    }

    @Override
    public void start() {
        tickService.addObserver(createOdometerTask());
    }

    private OdometerTask createOdometerTask() {
        return new OdometerTask(publisherService, totalCountSensor, appLifetimeTotalCount);
    }

    @Override
    public void stop() {
        final var template = new OdometerInstrumentQueryTemplateImpl();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(template.getTerminationQuery(getOdometerValue()));
        } catch (SQLException sqlException) {
            throw new RuntimeException(sqlException);
        }
    }

    private String getOdometerValue() {
        final var odometerTask = createOdometerTask();
        final var dataFrame = odometerTask.getCurrentDataFrame();
        final var distance = dataFrame.distance();
        return Double.toString(distance);
    }
}
