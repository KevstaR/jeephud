package com.kluke.jeephud.backend;

import com.kluke.jeephud.backend.instruments.InstrumentManager;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.ServiceManager;

import javax.inject.Inject;
import java.io.IOException;

public class ServerImpl implements Server {
    private boolean running = true;

    private final EventPublisher eventPublisher;
    private final ServiceManager serviceManager;
    private final InstrumentManager instrumentManager;

    @Inject
    public ServerImpl(EventPublisher eventPublisher,
                      ServiceManager serviceManager,
                      InstrumentManager instrumentManager) {
        this.eventPublisher = eventPublisher;
        this.serviceManager = serviceManager;
        this.instrumentManager = instrumentManager;
    }

    public void start() {
        instrumentManager.startInstruments();
        serviceManager.startServices();

        while (running) {
            try {
                eventPublisher.poll();
            } catch (IOException | InterruptedException ex) {
                stop();
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void stop() {
        running = false;
        eventPublisher.stop();
        serviceManager.stopServices();
        instrumentManager.stopInstruments();
    }
}
