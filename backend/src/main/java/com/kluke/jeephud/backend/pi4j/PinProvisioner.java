package com.kluke.jeephud.backend.pi4j;

import com.kluke.jeephud.backend.pi4j.pins.Pin;
import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.IOType;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalInputConfig;
import com.pi4j.io.gpio.digital.DigitalInputProvider;

public class PinProvisioner {
    private final Context pi4j;
    private final DigitalInputProvider provider;

    public PinProvisioner() {
        pi4j = Pi4J.newAutoContext();
        provider = pi4j.provider(IOType.DIGITAL_INPUT);
    }

    public DigitalInput provision(Pin pin) {
        var config = getConfig(pin);
        return provider.create(config);
    }

    private DigitalInputConfig getConfig(Pin pin) {
        return DigitalInput.newConfigBuilder(pi4j)
                .id(pin.id().name())
                .address(pin.address())
                .pull(pin.pullResistance())
                .build();
    }
}
