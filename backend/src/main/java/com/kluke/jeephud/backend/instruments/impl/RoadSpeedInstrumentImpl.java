package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.instruments.RoadSpeedInstrument;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.TickService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

public class RoadSpeedInstrumentImpl implements RoadSpeedInstrument {

    private final Sensor<List<Long>> roadSpeedSensor;
    private final TickService tickService;
    private final EventPublisher publisherService;

    private boolean initialized = false;

    @Inject
    public RoadSpeedInstrumentImpl(@Named("roadSpeedSensor") Sensor<List<Long>> roadSpeedSensor,
                                   TickService tickService,
                                   EventPublisher publisherService) {
        this.roadSpeedSensor = roadSpeedSensor;
        this.tickService = tickService;
        this.publisherService = publisherService;
    }

    @Override
    public void start() {
        if (!initialized) {
            tickService.addObserver(new RoadSpeedTask(roadSpeedSensor, publisherService));
            roadSpeedSensor.start();
            initialized = true;
        }
    }
}
