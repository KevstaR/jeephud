package com.kluke.jeephud.backend.services;

import javax.inject.Inject;

public class StandardTickService extends AbstractTickService {

    /**
     * Service schedules a task that runs on its own virtual thread.
     *
     * The standard implementation of a tick service `ticks` every 200ms. This timing must not be changed as it
     * directly impacts the road speed and sensor calculations.
     */
    @Inject
    public StandardTickService() {
        super(200);
    }
}
