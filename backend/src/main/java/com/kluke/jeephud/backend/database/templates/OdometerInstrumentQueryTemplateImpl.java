package com.kluke.jeephud.backend.database.templates;

import javax.inject.Inject;

public class OdometerInstrumentQueryTemplateImpl implements OdometerInstrumentQueryTemplate {
    private static final String TABLE_ID = "OdometerInstrument";
    private static final String VALUE_KEY = "value";

    @Inject
    public OdometerInstrumentQueryTemplateImpl() {}

    @Override
    public String getInitializationQuery() {
        return STR."SELECT \{VALUE_KEY} FROM \{TABLE_ID} ORDER BY timestamp DESC LIMIT 1";
    }

    @Override
    public String getTerminationQuery(String odometerValue) {
        return STR."INSERT INTO \{TABLE_ID} (\{VALUE_KEY}, timestamp) VALUES (\{odometerValue}, CURRENT_TIMESTAMP);";
    }
}
