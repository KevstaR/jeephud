package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.events.EventProvider;
import com.kluke.jeephud.backend.events.MillisecondEventProvider;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public interface EventsModule {
    @Binds
    @Singleton
    @Named("millisecondEventProvider")
    EventProvider bindsMillisecondEventProvider(MillisecondEventProvider impl);
}
