package com.kluke.jeephud.backend.services;

import com.kluke.jeephud.backend.events.EventSource;
import com.kluke.jeephud.backend.Lifecycle;

public interface TickService extends Lifecycle, EventSource {}
