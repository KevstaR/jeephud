package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.shared.data.OdometerDataFrame;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Math.PI;

public class OdometerTask implements Runnable {
    /**
     * Represents the historical value of the odometer at the time the vehicle's old odometer was removed and the new
     * odometer was started.
     * <P>
     * IMPORTANT: This value represents the total STATE CHANGES over the life of the vehicle, NOT mileage.
     * <P>
     * This value was calculated using a previous mileage of 150,000:
     * 150,000=x*DISTANCE_OF_MILE_TRAVELED
     * x=150,000/DISTANCE_OF_MILE_TRAVELED
     * x=HISTORICAL_RAW_ODOMETER_STATE_CHANGES state changes
     * <P>
     * META NOTE:
     * This value may as well be saved in a database and looked up, but the goal of this application is to minimize
     * start time, so hard coding as a private class instrument static value is significantly more performant and
     * manageable as long as consistency is kept across all instrument implementations.
     */
    private static final long HISTORICAL_RAW_ODOMETER_STATE_CHANGES = 1_536_243_089L;

    private static final float WHEEL_SIZE_INCHES = 32.0F;
    private static final float INCHES_IN_MILE = 63_360.0F;
    private static final float SENSOR_STATE_CHANGES_PER_ROTATION = 16.25F;
    private static final double WHEEL_CIRCUMFERENCE_INCH = WHEEL_SIZE_INCHES * PI;
    private static final double INCHES_PER_STATE_CHANGE = WHEEL_CIRCUMFERENCE_INCH / SENSOR_STATE_CHANGES_PER_ROTATION;

    /**
     * Distance per state change (approximate).
     */
    private static final double DISTANCE_OF_MILE_TRAVELED = INCHES_PER_STATE_CHANGE / INCHES_IN_MILE;

    private final EventPublisher publisherService;
    private final Sensor<Long> totalCountSensor;
    private final AtomicLong appLifetimeTotalCount;

    public OdometerTask(EventPublisher publisherService,
                        Sensor<Long> totalCountSensor,
                        AtomicLong appLifetimeTotalCount) {
        this.publisherService = publisherService;
        this.totalCountSensor = totalCountSensor;
        this.appLifetimeTotalCount = appLifetimeTotalCount;
    }

    @Override
    public void run() {
        publisherService.addEvent(getCurrentDataFrame());
    }

    public OdometerDataFrame getCurrentDataFrame() {
        final var distance = computeOdometerDistance();
        final var time = ZonedDateTime.now().toString();
        return new OdometerDataFrame(distance, time);
    }

    /**
     * @return
     *  + The historical sensor state changes unit.
     *  + The applications lifetime serialized sensor state changes unit.
     *  + The applications current running lifetime sensor state changes unit.
     *  x The coefficient which converts the abstract sensor state changes unit to an actual mileage.
     */
    private double computeOdometerDistance() {
        return (HISTORICAL_RAW_ODOMETER_STATE_CHANGES
                + appLifetimeTotalCount.get()
                + totalCountSensor.getData())
                * DISTANCE_OF_MILE_TRAVELED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OdometerTask that = (OdometerTask) o;
        return Objects.equals(publisherService, that.publisherService)
            && Objects.equals(totalCountSensor, that.totalCountSensor)
            && Objects.equals(appLifetimeTotalCount, that.appLifetimeTotalCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publisherService, totalCountSensor, appLifetimeTotalCount);
    }
}
