package com.kluke.jeephud.backend.datastructures;

import java.util.ArrayList;
import java.util.List;

public class DataStructureUtil {

    private DataStructureUtil() {}

    /**
     * This is used to count how many changes occurred within a given time delta.
     * The basic logic is:
     * If a valid (positive non-zero) delta has occurred, do a count
     * The list is modified.
     */
    public static long queryDeltaCount(List<Long> list, int range) {
        if (range < 0) throw new IllegalArgumentException("'range' cannot be negative.");

        // Track the total delta, so we know when we have reached the range
        var totalDelta = 0L;

        // Count each valid delta to return the number of deltas within the range
        var count = 0L;

        // Track the entries, so we know which ones to remove after they are successfully counted
        final List<Long> itemsInRange = new ArrayList<>(list.size());

        for (int i = 0; i < list.size() - 1; i++) {
            var first = list.get(i);
            var second = list.get(i + 1);
            var delta = second - first;

            if (totalDelta + delta > range) {
                // We are done
                break;
            } else {
                totalDelta += delta;
                // Only count when a valid delta has occurred
                if (delta > 0) {
                    count++;
                    itemsInRange.add(first);
                    itemsInRange.add(second);
                }
            }
        }

        // Remove successfully counted entries from the main list
        list.removeAll(itemsInRange);

        return count;
    }
}