package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.database.InstrumentVariableInitializer;
import com.kluke.jeephud.backend.database.InstrumentVariableInitializerImpl;
import com.kluke.jeephud.backend.instruments.OdometerInstrument;
import com.kluke.jeephud.backend.instruments.RoadSpeedInstrument;
import com.kluke.jeephud.backend.instruments.impl.OdometerInstrumentImpl;
import com.kluke.jeephud.backend.instruments.impl.RoadSpeedInstrumentImpl;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface InstrumentModule {
    @Binds
    @Singleton
    RoadSpeedInstrument bindRoadSpeedInstrument(RoadSpeedInstrumentImpl roadSpeedInstrument);

    @Binds
    @Singleton
    OdometerInstrument bindOdometerInstrument(OdometerInstrumentImpl odometerInstrument);

    // TODO: Replace scope to init scope?
    @Binds
    @Singleton
    InstrumentVariableInitializer bindInstrumentVariableInitializer(InstrumentVariableInitializerImpl instrumentVariableInitializer);
}
