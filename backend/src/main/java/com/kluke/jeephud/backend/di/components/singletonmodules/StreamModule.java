package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.socket.SocketApi;
import com.kluke.jeephud.backend.socket.ApiStream;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface StreamModule {
    @Binds
    @Singleton
    ApiStream provideStreamProvider(SocketApi impl);
}
