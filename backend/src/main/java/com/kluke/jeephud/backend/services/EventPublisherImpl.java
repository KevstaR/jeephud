package com.kluke.jeephud.backend.services;

import com.kluke.jeephud.backend.socket.ApiStream;

import javax.inject.Inject;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Publishes Instrument outputs against an OutputStream. This impl publisher acts as the primary API layer.
 * <P>
 * Within the context of this application, instruments subscribe and publish their Record events to an underlying
 * BlockingDeque. This publisher listens `events.take()` (blocking) to that queue and writes all events to the
 * OutputStream as they come in. The OutputStream acts as the backend abstraction as the application is push-forward
 * driven (meaning the backend pushes data to the frontend).
 */
public class EventPublisherImpl implements EventPublisher {

    private final ObjectOutputStream out;
    private final BlockingDeque<Record> events;
    private final ApiStream apiStream;

    @Inject
    public EventPublisherImpl(ApiStream apiStream) {
        this.apiStream = apiStream;
        this.out = apiStream.getOutputStream();
        this.events = new LinkedBlockingDeque<>();
    }

    @Override
    public void poll() {
        try {
            final Record event = events.take();
            out.writeObject(event);
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void addEvent(Record record) {
        events.push(record);
    }

    @Override
    public void stop() {
        apiStream.close();
    }
}
