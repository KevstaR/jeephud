package com.kluke.jeephud.backend.di.components;

import com.kluke.jeephud.backend.di.components.singletonmodules.DatabaseModule;
import com.kluke.jeephud.backend.di.components.singletonmodules.*;
import com.kluke.jeephud.backend.instruments.InstrumentManager;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.ServiceManager;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(
        modules = {
                QueryModule.class,
                PropertiesModule.class,
                DatabaseModule.class,
                InstrumentModule.class,
                ManagerModule.class,
                PinConfigurationModule.class,
                PinModule.class,
                SensorModule.class,
                ServiceModule.class,
                StreamModule.class,
                DatabaseModule.class,
                ServerModule.class,
                EventsModule.class
    }
)
public interface SingletonComponent {
    EventPublisher eventPublisher();
    ServiceManager serviceManager();
    InstrumentManager instrumentManager();
}
