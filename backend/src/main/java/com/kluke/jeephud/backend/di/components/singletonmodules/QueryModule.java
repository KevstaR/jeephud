package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.database.templates.OdometerInstrumentQueryTemplate;
import com.kluke.jeephud.backend.database.templates.OdometerInstrumentQueryTemplateImpl;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
public interface QueryModule {
    @Binds
    @Singleton
    OdometerInstrumentQueryTemplate bindOdometerInstrumentQueryTemplate(OdometerInstrumentQueryTemplateImpl impl);
}
