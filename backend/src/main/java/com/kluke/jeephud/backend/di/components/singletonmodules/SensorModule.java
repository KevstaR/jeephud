package com.kluke.jeephud.backend.di.components.singletonmodules;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.sensors.impl.RoadSpeedSensor;
import com.kluke.jeephud.backend.sensors.impl.TotalCountSensor;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

@Module
public interface SensorModule {
    @Binds
    @Singleton
    @Named("roadSpeedSensor")
    Sensor<List<Long>> bindsRoadSpeedSensor(RoadSpeedSensor impl);

    @Binds
    @Singleton
    @Named("totalCountSensor")
    Sensor<Long> bindsTotalCountSensor(TotalCountSensor impl);
}
