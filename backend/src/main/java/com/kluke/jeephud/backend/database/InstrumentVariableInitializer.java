package com.kluke.jeephud.backend.database;

import com.kluke.jeephud.backend.events.ConsumerEventSource;

public interface InstrumentVariableInitializer extends ConsumerEventSource<InstrumentVariables> {
    Thread start();
    void stop();
}
