package com.kluke.jeephud.backend.datastructures;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class EvictingArrayListTest {

    @Test
    void pushEvictsOldElements() {
        // Given: a deque with a max fixed capacity of 3
        final List<Integer> deque = new EvictingArrayList<>(3);

        // When: push 4 elements onto deque
        for (int i = 0; i < 4; i++) {
            deque.addFirst(i);
        }

        // Then: assert the 4th element was evicted and the deque is now empty
        assertThat(deque.removeFirst()).isEqualTo(3);
        assertThat(deque.removeFirst()).isEqualTo(2);
        assertThat(deque.removeFirst()).isEqualTo(1);

        assertThat(deque.size()).isEqualTo(0);
    }
}