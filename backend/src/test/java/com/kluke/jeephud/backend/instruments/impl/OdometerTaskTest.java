package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OdometerTaskTest {

    @Mock
    EventPublisher publisherService;

    @Mock
    Sensor<Long> totalCountSensor;

    @Test
    void run_adds_event() {
        // Given:
        when(totalCountSensor.getData()).thenReturn(0L);
        var task = new OdometerTask(publisherService, totalCountSensor, new AtomicLong(0L));

        // When:
        task.run();

        // Then:
        verify(publisherService).addEvent(isA(Record.class));
    }

    @Test
    void task_equals() {
        var appLifetimeTotalCount = new AtomicLong(0);
        var thisTask = new OdometerTask(publisherService, totalCountSensor, appLifetimeTotalCount);
        var thatTask = new OdometerTask(publisherService, totalCountSensor, appLifetimeTotalCount);

        assertThat(thisTask).isEqualTo(thatTask);
    }

    @Test
    void task_not_equals() {
        var appLifetimeTotalCount = new AtomicLong(0);
        var thisTask = new OdometerTask(publisherService, totalCountSensor, new AtomicLong(0));
        var thatTask = new OdometerTask(publisherService, totalCountSensor, appLifetimeTotalCount);

        assertThat(thisTask).isNotEqualTo(thatTask);
    }
}