package com.kluke.jeephud.backend.di;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public class TestPropertiesModule {
    @Provides
    @Named("databaseUrl")
    @Singleton
    String provideDatabaseUrl() {
        return "jdbc:sqlite::memory:";
    }
}
