package com.kluke.jeephud.backend.di;

import com.kluke.jeephud.backend.Server;
import com.kluke.jeephud.backend.database.InstrumentVariableInitializer;
import com.kluke.jeephud.backend.di.components.SingletonComponent;
import com.kluke.jeephud.backend.di.components.singletonmodules.*;
import com.kluke.jeephud.backend.instruments.OdometerInstrument;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.sensors.impl.RoadSpeedSensor;
import com.kluke.jeephud.backend.sensors.impl.TotalCountSensor;
import com.kluke.jeephud.backend.services.TickService;
import com.kluke.jeephud.backend.socket.ApiStream;
import com.pi4j.io.gpio.digital.DigitalInput;
import dagger.Component;

import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.Connection;
import java.util.List;

@Singleton
@Component(
        modules = {
                QueryModule.class,
                TestPropertiesModule.class,
                DatabaseModule.class,
                InstrumentModule.class,
                ManagerModule.class,
                PinConfigurationModule.class,
                FakePinModule.class,
                SensorModule.class,
                ServiceModule.class,
                FakeStreamModule.class,
                DatabaseModule.class,
                ServerModule.class,
                TestEventsModule.class
        })
public interface TestComponent extends SingletonComponent {
    // Publishing
    ApiStream apiStream();

    // Pins
    @Named("roadSpeedPin")
    DigitalInput roadSpeedPin();

    // Sensors
    @Named("roadSpeedSensor")
    Sensor<List<Long>> roadSpeedSensor();
    @Named("totalCountSensor")
    Sensor<Long> totalCountSensor();

    // Instruments
    OdometerInstrument odometerInstrument();

    // Server
    Server server();

    // Database
    Connection connection();

    // Tick
    TickService tickService();

    // Initializer
    InstrumentVariableInitializer instrumentVariableInitializer();
}
