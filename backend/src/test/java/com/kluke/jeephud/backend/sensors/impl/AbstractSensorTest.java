package com.kluke.jeephud.backend.sensors.impl;

import com.kluke.jeephud.backend.di.DaggerTestComponent;
import com.kluke.jeephud.backend.di.TestComponent;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInput;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

public abstract class AbstractSensorTest {
    protected MockDigitalInput mockRoadSpeedPin;
    protected Sensor<List<Long>> roadSpeedSensor;
    protected Sensor<Long> totalCountSensor;

    @BeforeEach
    public void beforeEach() {
        final TestComponent testComponent = DaggerTestComponent.create();
        this.roadSpeedSensor = testComponent.roadSpeedSensor();
        this.totalCountSensor = testComponent.totalCountSensor();
        this.mockRoadSpeedPin = (MockDigitalInput) testComponent.roadSpeedPin();
    }
}
