package com.kluke.jeephud.backend.sensors.impl;

import com.pi4j.io.gpio.digital.DigitalState;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RoadSpeedSensorTest extends AbstractSensorTest {
    @Test
    public void sensor_populates_data() {
        // When: start the sensor and send some mock data to the pin
        roadSpeedSensor.start();

        for (int i = 0; i < 10; i++) {
            var state = mockRoadSpeedPin.state();
            mockRoadSpeedPin.mockState(DigitalState.getInverseState(state));
        }

        // Then: query data
        assertThat(roadSpeedSensor.getData().size()).isEqualTo(10);
    }
}