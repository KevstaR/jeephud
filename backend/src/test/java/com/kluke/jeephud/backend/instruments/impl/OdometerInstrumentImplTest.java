package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.database.InstrumentVariableInitializer;
import com.kluke.jeephud.backend.di.DaggerTestComponent;
import com.kluke.jeephud.backend.di.TestComponent;
import com.kluke.jeephud.backend.instruments.OdometerInstrument;
import com.kluke.jeephud.backend.mocks.FakeApiStream;
import com.kluke.jeephud.backend.pinmocks.MockPinStateChanger;
import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.TickService;
import com.kluke.jeephud.shared.data.OdometerDataFrame;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInput;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import static com.kluke.jeephud.backend.database.DatabaseHelper.getLatestOdometerValue;
import static com.kluke.jeephud.backend.database.DatabaseHelper.preLoadTestDatabase;
import static org.assertj.core.api.Assertions.assertThat;

class OdometerInstrumentImplTest {
    // Logging
    private static final Logger LOGGER = Logger.getLogger(OdometerInstrumentImplTest.class.getName());

    // DI Test Context
    private TestComponent daggerTestComponent;

    // Create SUT
    private OdometerInstrument sut;

    // Sensors
    private Sensor<Long> totalCountSensor;

    // Real services used to manipulate the SUT
    private TickService tickService;
    private EventPublisher eventPublisher;
    private InstrumentVariableInitializer instrumentVariableInitializer;

    // Fake API Stream to read output Records
    private FakeApiStream fakeApiStream;

    // Mock the RPi pin to manipulate the SUT input
    private MockPinStateChanger mockPinStateChanger;

    // In-memory database connection. When connection is closed, the database drops.
    private Connection connection;

    @BeforeEach
    void setUp() {
        daggerTestComponent = DaggerTestComponent.create();
        totalCountSensor = daggerTestComponent.totalCountSensor();
        sut = daggerTestComponent.odometerInstrument();
        tickService = daggerTestComponent.tickService();
        eventPublisher = daggerTestComponent.eventPublisher();
        fakeApiStream = (FakeApiStream) daggerTestComponent.apiStream();
        instrumentVariableInitializer = daggerTestComponent.instrumentVariableInitializer();

        final MockDigitalInput mockRoadSpeedPin = (MockDigitalInput) daggerTestComponent.roadSpeedPin();
        mockPinStateChanger = new MockPinStateChanger(mockRoadSpeedPin);

        connection = daggerTestComponent.connection();
    }

    @AfterEach
    void tearDown() throws SQLException {
        connection.close();
    }

    @Test
    void start_loads_value_from_database() throws Exception {
        // Given:
        final var distanceTraveledMultiplier = 0.00009764079731436809D;
        final var appDistance = 1_000_000L;
        final var sqlCommands = List.of("""
           CREATE TABLE OdometerInstrument (
               id INTEGER PRIMARY KEY,
               value INTEGER NOT NULL,
               timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
           );""",
           STR."INSERT INTO OdometerInstrument (value, timestamp) VALUES (\{appDistance}, CURRENT_TIMESTAMP);"
        );
        preLoadTestDatabase(connection, sqlCommands);

        // When:
        /*
         * Call start in instrument variable initializer before start on sut to init value. The instrument variable
         * initializer is a global initializer for all classes called externally to reduce the db read overhead on start.
         *
         * In the test, we block with join().
         */
        instrumentVariableInitializer.start().join();

        // Call start first to add observer to tick service
        sut.start();
        generateDataFrame();

        // Then:
        final Record record = extractRecord();
        assertThat(record).isInstanceOf(OdometerDataFrame.class);
        final OdometerDataFrame odometerDataFrame = (OdometerDataFrame) record;

        LOGGER.info(STR."Odometer Data Frame: \{odometerDataFrame.toString()}");

        final var expectedDistanceFromDbLoad = distanceTraveledMultiplier * appDistance; // 97.64079731436809
        final var expected = 150_000.0 + expectedDistanceFromDbLoad;

        LOGGER.info(STR."Expected distance from db load: \{expectedDistanceFromDbLoad} ; Expected Distance: \{expected}");

        assertThat(odometerDataFrame.distance()).isCloseTo(expected, Offset.offset(0.0001));
    }

    @Test
    void start_calculates_value_from_sensor() throws Exception {
        // Call start first to add observer to tick service
        sut.start();

        // Add observer to pin state change listener
        totalCountSensor.start();
        mockPinStateChanger.startMockPinStateChange(0, 100000);
        mockPinStateChanger.waitForAllTasksToComplete();

        // Manually notify observers one time only to generate a data frame
        generateDataFrame();

        final OdometerDataFrame record = (OdometerDataFrame) extractRecord();

        // Assert:
        assertThat(record.distance()).isCloseTo(150009.76415837917, Offset.offset(0.00000000001));
    }

    private void generateDataFrame() throws InterruptedException, IOException {
        // Manually notify observers one time only to generate a data frame
        tickService.notifyObservers();

        // Poll the event to write to output stream
        eventPublisher.poll();
    }

    @Test
    void stop_saves_value_to_database() throws Exception {
        // Given:
        final var sqlCommands = List.of("""
           CREATE TABLE OdometerInstrument (
               id INTEGER PRIMARY KEY,
               value INTEGER NOT NULL,
               timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
           );"""
        );
        preLoadTestDatabase(connection, sqlCommands);

        // When:
        /*
         * Call start in instrument variable initializer before start on sut to init value. The instrument variable
         * initializer is a global initializer for all classes called externally to reduce the db read overhead on start.
         *
         * In the test, we block with join().
         */
        instrumentVariableInitializer.start().join();

        // Call start first to add observer to tick service
        sut.start();

        // Add observer to pin state change listener
        totalCountSensor.start();
        mockPinStateChanger.startMockPinStateChange(0, 100000);
        mockPinStateChanger.waitForAllTasksToComplete();

        // Manually notify observers one time only to generate a data frame
        generateDataFrame();

        sut.stop();

        // Assert:

        final var query = STR."SELECT value FROM OdometerInstrument ORDER BY timestamp DESC LIMIT 1;";

        // TODO: Think about how we want to manage sql connections. It's just a simple file connection so possibly we make initializers maintain their own connections and not DI them.
        

        final var output = getLatestOdometerValue(connection, query);
        assertThat(output).isCloseTo(150009.76415837917, Offset.offset(0.00000000001));
    }

    private Record extractRecord() {
        try (final ObjectInput inputStream = new ObjectInputStream(new ByteArrayInputStream(fakeApiStream.byteArrayOutputStream.toByteArray()))) {
            return (Record) inputStream.readObject();
        } catch (IOException | ClassNotFoundException exception) {
            throw new RuntimeException(exception);
        }
    }
}