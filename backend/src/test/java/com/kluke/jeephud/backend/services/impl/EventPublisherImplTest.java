package com.kluke.jeephud.backend.services.impl;

import com.kluke.jeephud.backend.services.EventPublisher;
import com.kluke.jeephud.backend.services.EventPublisherImpl;
import com.kluke.jeephud.backend.socket.ApiStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.assertj.core.api.Assertions.assertThat;

class EventPublisherImplTest {
    private static final String EXPECTED_STRING = "hello";
    private static final long EXPECTED_LONG = 69L;

    private ByteArrayOutputStream out;
    private EventPublisher sut;

    @BeforeEach
    void beforeEach() {
        out = new ByteArrayOutputStream();
        sut = new EventPublisherImpl(mockStreamProvider());
    }

    @AfterEach
    void afterEach() throws IOException {
        out.close();
    }

    @Test
    void publisher_data() throws IOException, InterruptedException, ClassNotFoundException {
        // Given:
        sut.addEvent(new TestRecord(EXPECTED_STRING, EXPECTED_LONG));

        // When:
        sut.poll();

        // Then:
        final var outputObject = readObjectFromStream();
        assertThat(outputObject).isInstanceOf(TestRecord.class);

        final var record = (TestRecord) outputObject;
        assertThat(record.name()).isEqualTo(EXPECTED_STRING);
        assertThat(record.value()).isEqualTo(EXPECTED_LONG);
    }

    private record TestRecord(String name, float value) implements Serializable {}

    private Object readObjectFromStream() throws IOException, ClassNotFoundException {
        return new ObjectInputStream(new ByteArrayInputStream(out.toByteArray())).readObject();
    }

    private ApiStream mockStreamProvider() {
        return new ApiStream() {
            @Override
            public ObjectOutputStream getOutputStream() {
                try {
                    return new ObjectOutputStream(out);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void close() {
                try {
                    out.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }
}