package com.kluke.jeephud.backend.datastructures;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

class DataStructureUtilTest {

    @Test
    void list_only_has_many_items_with_range_smaller_than_available() {
        // Given: list with many time deltas
        final List<Long> list = getRealTestData();


        // When: query is performed with a range
        final long query = DataStructureUtil.queryDeltaCount(list, 100);

        // Then: query successfully returns a count of the items within that range and leaves the extras on the list
        System.out.println(STR."query = \{query}");
        System.out.println(STR."list = \{list}");
        assertThat(query).isEqualTo(9);
        assertThat(list).containsExactly(7313810L, 7313820L, 7313830L, 7313840L, 7313850L, 7313860L, 7313870L, 7313881L, 7313891L, 7313901L);
    }

    @NotNull
    private static List<Long> getRealTestData() {
        final List<Long> list = new EvictingArrayList<>(128);
        list.add(7313708L);
        list.add(7313719L); // 11
        list.add(7313729L); // 21
        list.add(7313739L); // 31
        list.add(7313749L); // 41
        list.add(7313759L); // 51
        list.add(7313769L); // 61
        list.add(7313779L); // 71
        list.add(7313789L); // 81
        list.add(7313799L); // 91 <<<<<< HERE
        list.add(7313810L); // 102
        list.add(7313820L); // 112
        list.add(7313830L); // 122
        list.add(7313840L); // 132
        list.add(7313850L); // 142
        list.add(7313860L); // 152
        list.add(7313870L); // 162
        list.add(7313881L); // 173
        list.add(7313891L); // 183
        list.add(7313901L); // 193
        return list;
    }

    @Test
    void list_has_many_items_and_the_query_range_is_met__expect_to_return_a_valid_query_with_leftover_items_in_deque() {
        // Given: list with many time deltas
        final List<Long> list = new EvictingArrayList<>(128);
        list.add(10L);
        list.add(20L); // 10
        list.add(30L); // 10
        list.add(35L); // 5
        list.add(45L); // 10
        list.add(55L); // 10 <<<< Query range of 50 should end here
        list.add(75L); // 20

        // When: query is performed with a range
        final long query = DataStructureUtil.queryDeltaCount(list, 50);

        // Then: query successfully returns a count of the items within that range and leaves the extras on the list
        assertThat(query).isEqualTo(5);
        assertThat(list).containsExactly( 75L);
    }

    @Test
    void list_only_has_one_item() {
        // Given: a deque with 1 item
        final List<Long> list = new EvictingArrayList<>(1);
        list.add(5L);

        // When: query is performed
        final long query = DataStructureUtil.queryDeltaCount(list, 10);

        // Then:
        assertThat(query).isEqualTo(0);
    }

    @Test
    void list_only_has_two_items() {
        // Given: a deque with 2 items
        final List<Long> list = new EvictingArrayList<>(2);
        list.add(50L);
        list.add(60L); // 10

        // When: a query with a range greater than what's in the queue is made
        final long query = DataStructureUtil.queryDeltaCount(list, 100);

        // Then: we should take whatever we can
        assertThat(query).isEqualTo(1);
    }
//
    @Test
    void list_only_has_two_items_with_range_smaller_than_available() {
        // Given: a deque with 2 items
        final List<Long> list = new EvictingArrayList<>(2);
        list.add(50L);
        list.add(60L); // 10

        // When: a query with a range less than what's in the queue is made
        final long query = DataStructureUtil.queryDeltaCount(list, 5);

        // Then: we shouldn't be able to take any values
        assertThat(query).isEqualTo(0);
        assertThat(list.stream().toList()).containsExactly(50L, 60L);
    }

    @Test
    void list_is_empty() {
        // Given: an empty deque
        final List<Long> list = new EvictingArrayList<>(100);

        // When: a query with any non-negative range is made
        final long query = DataStructureUtil.queryDeltaCount(list, 100);

        // Then: we should return 0
        assertThat(query).isEqualTo(0);
    }

    @Test
    void negative_query_input() {
        // Given: any queue
        final List<Long> list = new EvictingArrayList<>(10);

        // Then: should throw
        assertThatIllegalArgumentException().isThrownBy(() -> DataStructureUtil.queryDeltaCount(list, -100));
    }
}