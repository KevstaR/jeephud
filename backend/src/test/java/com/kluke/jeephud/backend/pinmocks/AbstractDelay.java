package com.kluke.jeephud.backend.pinmocks;

public abstract class AbstractDelay implements DelayTimer {
    public void sleep() {
        try {
            Thread.sleep(delay());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public abstract int delay();
}
