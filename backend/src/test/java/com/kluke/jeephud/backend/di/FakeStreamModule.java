package com.kluke.jeephud.backend.di;

import com.kluke.jeephud.backend.mocks.FakeApiStream;
import com.kluke.jeephud.backend.socket.ApiStream;
import dagger.Binds;
import dagger.Module;

import javax.inject.Singleton;

@Module
interface FakeStreamModule {
    @Binds
    @Singleton
    ApiStream provideStreamProvider(FakeApiStream impl);
}
