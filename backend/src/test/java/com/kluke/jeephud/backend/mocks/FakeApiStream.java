package com.kluke.jeephud.backend.mocks;

import com.kluke.jeephud.backend.socket.ApiStream;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class FakeApiStream implements ApiStream {
    public final ByteArrayOutputStream byteArrayOutputStream;
    private final ObjectOutputStream out;

    @Inject
    public FakeApiStream() {
        this.byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.out = new ObjectOutputStream(byteArrayOutputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ObjectOutputStream getOutputStream() {
        return out;
    }

    @Override
    public void close() {
        try {
            out.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
