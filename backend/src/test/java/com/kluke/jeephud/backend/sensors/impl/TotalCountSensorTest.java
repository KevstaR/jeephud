package com.kluke.jeephud.backend.sensors.impl;

import com.pi4j.io.gpio.digital.DigitalState;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TotalCountSensorTest extends AbstractSensorTest {

    @Test
    public void sensor_value_is_called_without_any_sensor_state_changes() {
        // When:
        totalCountSensor.start();

        // Then: query data
        assertThat(totalCountSensor.getData()).isEqualTo(0L);
    }

    @Test
    public void sensor_value_is_called_with_sensor_data() {
        // When:
        totalCountSensor.start();
        for (int i = 0; i < 10; i++) {
            mockRoadSpeedPin.mockState(DigitalState.getInverseState(mockRoadSpeedPin.state()));
        }

        // Then:
        assertThat(totalCountSensor.getData()).isEqualTo(10L);
    }
}
