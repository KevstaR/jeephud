package com.kluke.jeephud.backend.instruments.impl;

import com.kluke.jeephud.backend.sensors.Sensor;
import com.kluke.jeephud.backend.sensors.impl.RoadSpeedSensor;
import com.kluke.jeephud.backend.services.EventPublisher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RoadSpeedTaskTest {

    @Mock
    EventPublisher publisherService;

    @Mock
    Sensor<List<Long>> sensor;

    @Test
    void run_adds_event() {
        // Given:
        when(sensor.getData()).thenReturn(Collections.singletonList(69L)); // Mutable list required!
        var task = new RoadSpeedTask(sensor, publisherService);

        // When:
        task.run();

        // Then:
        verify(publisherService).addEvent(isA(Record.class));
    }

    @Test
    void task_equals() {
        var thisTask = new RoadSpeedTask(sensor, publisherService);
        var thatTask = new RoadSpeedTask(sensor, publisherService);

        assertThat(thisTask).isEqualTo(thatTask);
    }

    @Test
    void task_not_equals() {
        var thisTask = new RoadSpeedTask(new RoadSpeedSensor(mock(), mock()), publisherService);
        var thatTask = new RoadSpeedTask(sensor, publisherService);

        assertThat(thisTask).isNotEqualTo(thatTask);
    }
}
