package com.kluke.jeephud.backend;

import com.kluke.jeephud.backend.di.DaggerTestComponent;
import com.kluke.jeephud.backend.di.TestComponent;
import com.kluke.jeephud.backend.mocks.FakeApiStream;
import com.kluke.jeephud.backend.pinmocks.MockPinStateChanger;
import com.kluke.jeephud.backend.pinmocks.RandomDelayRange;
import com.kluke.jeephud.shared.data.OdometerDataFrame;
import com.kluke.jeephud.shared.data.RoadSpeedDataFrame;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;

class ServerTest {
    private static final Logger LOGGER = Logger.getLogger(ServerTest.class.getName());

    private Server sut;
    private MockPinStateChanger mockPinStateChanger;
    private FakeApiStream fakeApiStream;

    @BeforeEach
    public void beforeEach() {
        final TestComponent component = DaggerTestComponent.create();
        sut = component.server();

        final MockDigitalInput mockRoadSpeedPin = (MockDigitalInput) component.roadSpeedPin();

        this.mockPinStateChanger = new MockPinStateChanger(mockRoadSpeedPin);
        this.fakeApiStream = (FakeApiStream) component.apiStream();
    }

    @Test
    void start_stop() {
        mockPinStateChanger.startMockPinStateChange(new RandomDelayRange(5, 50));

        scheduleTermination(sut, mockPinStateChanger);

        sut.start();

        assertRecordsInStream();
    }

    /**
     * This test asserts that only implemented records exist in the stream.
     * If a new record type is added, it should be asserted here.
     */
    @Test
    void assert_only_specific_records_exist_in_stream() {
        mockPinStateChanger.startMockPinStateChange(new RandomDelayRange(5, 50));

        scheduleTermination(sut, mockPinStateChanger);

        sut.start();

        assertSpecificRecordsInStream();
    }

    private void scheduleTermination(Server server, MockPinStateChanger mockPinStateChanger) {
        Thread.ofVirtual().start(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            server.stop();
            mockPinStateChanger.stopAllTasks();
        });
    }

    private void assertSpecificRecordsInStream() {
        boolean whileLoopIsReached = false;

        try (ObjectInput inputStream = new ObjectInputStream(new ByteArrayInputStream(fakeApiStream.byteArrayOutputStream.toByteArray()))) {
            // A set of all record types. This allows us to easily check if a type was called.
            final Set<Class<?>> records = new HashSet<>();

            Object obj;
            while ((obj = tryReadObject(inputStream)) != null) {
                whileLoopIsReached = true;

                switch (obj) {
                    case RoadSpeedDataFrame roadSpeedDataFrame -> records.add(roadSpeedDataFrame.getClass());
                    case OdometerDataFrame odometerDataFrame -> records.add(odometerDataFrame.getClass());
                    default -> {/* Do nothing */}
                }
            }

            assertThat(records)
                    .as("Each record type must exist in stream.")
                    .containsExactlyInAnyOrder(
                            RoadSpeedDataFrame.class,
                            OdometerDataFrame.class
                    );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        assertThat(whileLoopIsReached).isTrue();
    }

    private void assertRecordsInStream() {
        try (ObjectInput inputStream = new ObjectInputStream(new ByteArrayInputStream(fakeApiStream.byteArrayOutputStream.toByteArray()))) {
            Object obj;
            boolean atLeastOneRecordExists = false;
            while ((obj = tryReadObject(inputStream)) != null) {
                assertThat(obj).isInstanceOf(Record.class);
                LOGGER.info(obj.toString());
                atLeastOneRecordExists = true;
            }
            assertThat(atLeastOneRecordExists).as("At least one record exists in the event stream.").isTrue();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Object tryReadObject(ObjectInput objectInput) {
        Object object;
        try {
            object = objectInput.readObject();
        } catch (ClassNotFoundException | IOException e) {
            return null;
        }
        return object;
    }
}