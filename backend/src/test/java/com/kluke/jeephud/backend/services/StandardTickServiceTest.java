package com.kluke.jeephud.backend.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;

class StandardTickServiceTest {
    TickService tickService = null;
    AtomicInteger observerTriggeredCount = null;

    @BeforeEach
    public void beforeEach() {
        tickService = new StandardTickService();
        observerTriggeredCount = new AtomicInteger(0);
    }

    @Test
    public void tickService_notify_observer_test() {
        // When: We notify observers, increment.
        tickService.addObserver(observerTriggeredCount::incrementAndGet);

        // Note that we don't call start on the service
        tickService.notifyObservers();

        // Observer is only triggered once.
        assertThat(observerTriggeredCount).hasValue(1);
    }

    @Test
    public void tickService_notify_observer_start_test() {
        // Call start before we add observer
        tickService.start();

        // When: We notify observers, increment.
        tickService.addObserver(observerTriggeredCount::incrementAndGet);

        tickService.notifyObservers();

        // Observer is only triggered once.
        assertThat(observerTriggeredCount).hasValue(1);
    }
}