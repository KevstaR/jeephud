package com.kluke.jeephud.backend.di;


import com.kluke.jeephud.backend.events.EventProvider;
import com.kluke.jeephud.backend.events.TestEventProvider;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public interface TestEventsModule {
    @Binds
    @Singleton
    @Named("millisecondEventProvider")
    EventProvider provideEventProvider(TestEventProvider impl);
}
