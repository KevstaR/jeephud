package com.kluke.jeephud.backend.events;

import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

public class TestEventProvider implements EventProvider {
    private long total = 0L;

    @Inject
    public TestEventProvider() {
    }

    @Override
    public long getEvent() {
        return total += 20;
    }

    @Test
    public void getEvent_single() {
        // Given:
        final EventProvider sut = new TestEventProvider();

        // When:
        final long event = sut.getEvent();

        // Then:
        assertThat(event).isEqualTo(20L);
    }

    @Test
    public void getEvent_sums() {
        // Given:
        final EventProvider sut = new TestEventProvider();

        // When:
        final long event1 = sut.getEvent();
        final long event2 = sut.getEvent();
        final long event3 = sut.getEvent();

        // Then:
        assertThat(event1).isEqualTo(20L);
        assertThat(event2).isEqualTo(40L);
        assertThat(event3).isEqualTo(60L);
    }
}
