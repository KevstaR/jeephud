package com.kluke.jeephud.backend.services.impl;

import org.junit.jupiter.api.Test;

public class SpeedConversionTest {

    /**
     * Silly test to document the basic steps to get the deque size for the speed sensor.
     */
    @Test
    public void testSpeedConversion() {
        var speedConversionFactor = 1.125F;
        var maxSpeedMph = 100.0F;

        var maxDelta = maxSpeedMph / speedConversionFactor;

        var maxDeltaPerSecond = maxDelta * 5;

        System.out.println(maxDeltaPerSecond);
    }

    @Test
    public void testDistance() {
        var stateChanges = 16.25;
        var speedometerGearRatio = 34.0F / 13.0F;

        System.out.println(speedometerGearRatio);

        System.out.println(speedometerGearRatio / stateChanges);
    }
}
