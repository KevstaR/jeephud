package com.kluke.jeephud.backend.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DatabaseHelper {
    public static void preLoadTestDatabase(Connection connection, List<String> sqlCommands) {
        try (Statement statement = connection.createStatement()) {
            for (String sql : sqlCommands) {
                statement.executeUpdate(sql);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static double getLatestOdometerValue(Connection connection, String query) {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            if (resultSet.next()) {
                return resultSet.getDouble("value");
            } else {
                throw new RuntimeException("No rows found - query: " + query);  // No rows found
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
