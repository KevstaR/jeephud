package com.kluke.jeephud.backend.di;

import com.pi4j.Pi4J;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInput;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInputProvider;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;
import javax.inject.Singleton;

@Module
public class FakePinModule {
    @Provides
    @Singleton
    @Named("roadSpeedPin")
    static DigitalInput provideRoadSpeedPin() {
        return createMockPin(4);
    }

    private static MockDigitalInput createMockPin(int address) {
        return Pi4J
                .newAutoContext()
                .providers()
                .get(MockDigitalInputProvider.ID, MockDigitalInputProvider.class)
                .create(address);
    }
}
