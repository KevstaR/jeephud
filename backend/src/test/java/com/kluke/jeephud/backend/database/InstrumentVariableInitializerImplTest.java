package com.kluke.jeephud.backend.database;

import com.kluke.jeephud.backend.database.templates.OdometerInstrumentQueryTemplateImpl;
import com.kluke.jeephud.backend.di.DaggerTestComponent;
import com.kluke.jeephud.backend.di.TestComponent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import static com.kluke.jeephud.backend.database.DatabaseHelper.preLoadTestDatabase;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * These tests leverage the sqlite in memory database. As long as the initial connection is passed into the SUT,
 * the connection lifecycle will permit preloading and persisting data in memory for the duration of each test.
 * <p>
 * Each test receives a new in memory database.
 */
class InstrumentVariableInitializerImplTest {
    private TestComponent component;
    private Connection connection;
    private InstrumentVariableInitializer instrumentVariableInitializer;

    @BeforeEach
    public void beforeEach() {
        component = DaggerTestComponent.create();
        connection = component.connection();
    }

    @AfterEach
    public void afterEach() {
        instrumentVariableInitializer.stop();
    }

    @Test
    public void subscribe_and_notify() throws InterruptedException {
        // Given:
        instrumentVariableInitializer = new InstrumentVariableInitializerImpl(connection, new OdometerInstrumentQueryTemplateImpl());

        final var sqlCommands = List.of("""
            CREATE TABLE OdometerInstrument (
                id INTEGER PRIMARY KEY,
                value INTEGER NOT NULL,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            );""",
            "INSERT INTO OdometerInstrument (value, timestamp) VALUES (10000, CURRENT_TIMESTAMP);"
        );
        preLoadTestDatabase(connection, sqlCommands);

        final BlockingDeque<InstrumentVariables> blockingDeque = new LinkedBlockingDeque<>(1);

        // When:
        instrumentVariableInitializer.addObserver(blockingDeque::add);
        instrumentVariableInitializer.start();

        // Then:
        var instrumentVariables = blockingDeque.take();

        // Generic record
        assertThat(instrumentVariables).isNotNull().isInstanceOf(Record.class);

        // Individual values
        assertThat(instrumentVariables.rawOdometerValue()).isEqualTo(10_000L);
    }
}