package com.kluke.jeephud.backend.pinmocks;

import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.plugin.mock.provider.gpio.digital.MockDigitalInput;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MockPinStateChanger {
    private final MockDigitalInput pin;
    private final ExecutorService executor;
    private final Set<Future<?>> tasks;


    public MockPinStateChanger(MockDigitalInput pin) {
        this.pin = pin;
        this.executor = Executors.newVirtualThreadPerTaskExecutor();
        this.tasks = new HashSet<>();
    }

    public void startMockPinStateChange(DelayTimer delayTimer) {
        final Future<?> task = executor.submit(() -> changeState(delayTimer));
        tasks.add(task);
    }

    private void changeState(DelayTimer delayTimer) {
        while (true) {
            swapPinStates();
            sleep(delayTimer.delay());
        }
    }

    private static void sleep(int delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public void startMockPinStateChange(int delay, int count) {
        final Future<?> task = executor.submit(() -> changeState(delay, count));
        tasks.add(task);
    }

    private void changeState(int delay, int count) {
        for (int i = 0; i < count; i++) {
            swapPinStates();
            sleep(delay);
        }
    }

    private void swapPinStates() {
        pin.mockState(DigitalState.getInverseState(pin.state()));
    }

    public void stopAllTasks() {
        for (Future<?> task : tasks) {
            task.cancel(true);
        }
    }

    public void waitForAllTasksToComplete() {
        for (Future<?> task : tasks) {
            try {
                task.get(); // Block until the task completes or is canceled
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException("Task was interrupted", e);
            } catch (ExecutionException e) {
                throw new RuntimeException("Task encountered an exception", e);
            }
        }
    }
}