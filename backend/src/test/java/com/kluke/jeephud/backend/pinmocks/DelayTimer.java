package com.kluke.jeephud.backend.pinmocks;

public interface DelayTimer {
    int delay();
}
