package com.kluke.jeephud.backend.pinmocks;

import java.util.Random;

public class RandomDelayRange extends AbstractDelay {
    private final int low;
    private final int high;
    private final Random random;

    public RandomDelayRange(int low, int high) {
        this.low = low;
        this.high = high;
        random = new Random();
    }

    @Override
    public int delay() {
        return random.nextInt(high - low) + low;
    }
}
