plugins {
    java
    idea
}

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(22))
    }

    sourceCompatibility = JavaVersion.VERSION_22
    targetCompatibility = JavaVersion.VERSION_22
}

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(listOf("--enable-preview"))
}


tasks.withType<Test> {
    jvmArgs = (jvmArgs ?: listOf()) + listOf("--enable-preview")
}

tasks.withType<JavaExec> {
    jvmArgs = (jvmArgs ?: listOf()) + listOf("--enable-preview")
}



allprojects {
    group = "com.kluke.jeephud"
    version = "1.0-SNAPSHOT"
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    // ==== Main ====
    implementation(project(":shared"))

    val piVersion = "2.4.0"

    // Raspberry Pi
    implementation("com.pi4j:pi4j-core:$piVersion")
    implementation("com.pi4j:pi4j-plugin-raspberrypi:$piVersion")
    implementation("com.pi4j:pi4j-plugin-pigpio:$piVersion")
    implementation("com.pi4j:pi4j-device:1.3")

    // Backend Server
    implementation("io.javalin:javalin:5.6.3")

    // Logging
    implementation("org.slf4j:slf4j-simple:2.0.7")

    // SQLite
    implementation("org.xerial:sqlite-jdbc:3.45.1.0")

    // Dependency Injection
    val daggerVersion = "2.51.1"
    implementation("com.google.dagger:dagger:$daggerVersion")
    annotationProcessor("com.google.dagger:dagger-compiler:$daggerVersion")
    testAnnotationProcessor("com.google.dagger:dagger-compiler:$daggerVersion")

    // ==== Testing ====
    val mockitoVersion = "5.8.0"
    val junitVersion = "5.10.1"

    // Raspberry Pi Testing
    testImplementation("com.pi4j:pi4j-test:$piVersion")
    testImplementation("com.pi4j:pi4j-plugin-mock:$piVersion")

    // Testing Framework
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    // Mocking
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
    testImplementation("org.mockito:mockito-junit-jupiter:$mockitoVersion")

    // Assertions
    testImplementation("org.assertj:assertj-core:3.24.2")
}