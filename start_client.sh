ROOT_DIR=$(pwd)
pushd backend/build/classes/java/main/
  kotlin -cp $ROOT_DIR/shared/build/libs/shared-1.0-SNAPSHOT.jar:$ROOT_DIR/frontend/build/classes/kotlin/main com.kluke.jeephud.frontend.ClientKt
popd