CREATE TABLE InstrumentConstants (
    constant_key VARCHAR(255) PRIMARY KEY,
    constant_value INTEGER NOT NULL
);

CREATE TABLE OdometerInstrument (
    id INTEGER PRIMARY KEY,
    value INTEGER NOT NULL,
    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
);