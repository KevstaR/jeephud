#!/bin/bash
set -e

DATABASE_NAME=$1

sqlite3 "${DATABASE_NAME}.db" < schema.sql
sqlite3 "${DATABASE_NAME}.db" < preload.sql
