ROOT_DIR=$(pwd)
pushd backend/build/classes/java/main/
  java --enable-preview -cp $ROOT_DIR/shared/build/libs/shared-1.0-SNAPSHOT.jar:$ROOT_DIR/backend/build/classes/java/main com.kluke.jeephud.backend.Server
popd