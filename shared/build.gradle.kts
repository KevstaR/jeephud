plugins {
    java
}

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(21))
    }

    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

tasks.withType<JavaCompile> {
    options.compilerArgs.addAll(listOf("--enable-preview"))
}


tasks.withType<Test> {
    jvmArgs = (jvmArgs ?: listOf()) + listOf("--enable-preview")
}

tasks.withType<JavaExec> {
    jvmArgs = (jvmArgs ?: listOf()) + listOf("--enable-preview")
}



allprojects {
    group = "com.kluke.jeephud"
    version = "1.0-SNAPSHOT"
}

tasks.test {
    useJUnitPlatform()
}

dependencies {
    // ==== Main ====
    // ==== Testing ====
    val mockitoVersion = "5.8.0"
    val junitVersion = "5.10.1"

    // Testing Framework
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

    // Mocking
    testImplementation("org.mockito:mockito-core:$mockitoVersion")
    testImplementation("org.mockito:mockito-junit-jupiter:$mockitoVersion")

    // Assertions
    testImplementation("org.assertj:assertj-core:3.24.2")
}