package com.kluke.jeephud.shared.data;

import java.io.Serializable;

public record OdometerDataFrame(double distance, String timestamp) implements Serializable { }
