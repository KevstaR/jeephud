package com.kluke.jeephud.shared.data;

import java.io.Serializable;

public record RoadSpeedDataFrame(double mph, String timestamp) implements Serializable {}
